##Answer
Javascript array foreach is an inbuilt function that can be used to execute a function on each item in the array. 
The forEach() method is called on the array Object and is passed the function that is called on each item in the array.  The callback function can also take the second parameter of the index in case you need to reference the index of the current element in the array.